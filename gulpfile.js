var gulp = require("gulp");
var babel = require("gulp-babel");
var sass = require("gulp-sass");

sass.compiler = require("node-sass");

gulp.task("babel", function() {
  return gulp.src("js/*.js")
    .pipe(babel())
    .pipe(gulp.dest("dist/js"));
});

gulp.task("sass", function() {
  return gulp.src("sass/main.scss")
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest("dist/css"))
});

gulp.task("default", gulp.parallel("babel", "sass"));
