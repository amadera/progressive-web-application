const tmdbKey = '2a99cadf4d0ba51286a32c3d58dc0566';

let onClickMenuBtn = function () {
  const element = document.getElementById('main-header');
  const isMenuOpen = document.getElementsByClassName('is-menu-open');

  if (isMenuOpen.length) {
    element.classList.remove('is-menu-open');
  } else {
    element.classList.add('is-menu-open');
  }
};

let populateMoviesList = function (movies, type) {
  let destElement = document.getElementById('movies-list');
  let selecteType = document.getElementsByClassName('is-selected');

  if (selecteType.length) {
    selecteType[0].classList.remove('is-selected');
  }

  if (type) {
    document.getElementsByClassName(type)[0].classList.add('is-selected');
  }

  movies.forEach(movie => {
    let movieElement = document.createElement('li');
    movieElement.innerHTML += `<a href="./movieDetails.html?id=${movie.id}"><img src="https://image.tmdb.org/t/p/w500/${movie.poster_path}">`;
    movieElement.innerHTML += `<h3>${movie.title}</h3></a>`;
    destElement.appendChild(movieElement);
  });
};

let getMovies = function (type) {
  const url = `https://api.themoviedb.org/3/movie/${type}?api_key=${tmdbKey}&language=es-ES&page=1`;
  let destElement = document.getElementById('main-uocflix');
  destElement.innerHTML = '<img src="imgs/cargando.gif">';
  axios.get(url).then(res => {
    destElement.innerHTML = `<ul id="movies-list"></ul>`;
    populateMoviesList(res.data.results, type);
  }).catch(err => console.log(err));
};

let buscarPelis = function () {
  let querySearch = document.getElementsByClassName('search-box')[0].value;
  let destElement = document.getElementById('main-uocflix');
  destElement.innerHTML = '<img src="imgs/cargando.gif">';
  const url = `https://api.themoviedb.org/3/search/movie?api_key=${tmdbKey}&language=es-ES&page=1&query=${querySearch}`;
  axios.get(url).then(res => {
    destElement.innerHTML = `<div class="search-msg">Resultados de búsqueda por: ${querySearch}</div> <ul id="movies-list"></ul>`;
    querySearch = '';
    populateMoviesList(res.data.results);
  }).catch(err => console.log(err));
};

let onInit = function () {
  getMovies('popular');
}();