const tmdbKey = '2a99cadf4d0ba51286a32c3d58dc0566';

let getMoviesDetails = function (movieId) {
  const url = `https://api.themoviedb.org/3/movie/${movieId}?api_key=${tmdbKey}&language=es-ES`;
  let destElement = document.getElementById('header');
  destElement.style.display = 'block';
  destElement.innerHTML = '<img src="imgs/cargando.gif">';
  axios.get(url).then(res => {
    destElement.innerHTML = '';
    destElement.style.display = 'flex';
    populateMovieDetails(res.data);
  }).catch(err => console.log(err));
};

let getMovieImgs = function (movieId) {
  const url = `https://api.themoviedb.org/3/movie/${movieId}/images?api_key=${tmdbKey}&language=es-ES`;
  axios.get(url).then(res => {
    console.log(res.data); // populateMovieImages(res.data);
  }).catch(err => console.log(err));
};

let getSimilarMovies = function (movieId) {
  const url = `https://api.themoviedb.org/3/movie/${movieId}/similar?api_key=${tmdbKey}&language=en-US`;
  axios.get(url).then(res => {
    console.log(res.data);
    populateSimilarMovies(res.data.results.slice(0, 5));
  }).catch(err => console.log(err));
};

let populateMovieDetails = function (movie) {
  let genres = [];
  let mainContainer = document.getElementById('movie-info');
  movie.genres.forEach(genre => {
    genres.push(genre.name);
  });
  let header = document.getElementById('header');
  header.style.backgroundImage = `url("https://image.tmdb.org/t/p/w500${movie.poster_path}")`;
  header.innerHTML = `<div class="title"><h1>${movie.title}</h1></div>`;
  mainContainer.innerHTML = `<div class="overview"><h2>Resumen de la película</h2> ${movie.overview} </div>`;
  mainContainer.innerHTML += `<div class="details"> 
                                <h2>Detalles</h2> 
                                <ul> 
                                  <li><h3 class="details-title">Generos: </h3> ${genres.join(', ')}</li>
                                  <li><h3 class="details-title">Fecha de lanzamiento: </h3> ${new Date(movie.release_date).toLocaleDateString("es-ES")} </li>
                                  <li><h3 class="details-title">Duración: </h3> ${movie.runtime} min </li>
                                  <li><h3 class="details-title">Valoración media: </h3> ${movie.vote_average} </li>
                                </ul> 
                              </div>`;
};

let populateSimilarMovies = function (movies) {
  if (!movies) {
    return;
  }

  document.getElementsByClassName('similar-movies-wrapper')[0].innerHTML = '<h2>Películas similares</h2>';
  document.getElementsByClassName('similar-movies-wrapper')[0].innerHTML += '<ul id="similar-movies"></ul>';
  let mainContainer = document.getElementById('similar-movies');
  mainContainer.innerHTML = '';
  movies.forEach(movie => {
    let movieElement = document.createElement('li');
    movieElement.innerHTML = `<a href="./movieDetails.html?id=${movie.id}"><img src="https://image.tmdb.org/t/p/w500/${movie.poster_path}">`;
    movieElement.innerHTML += `<h3>${movie.title}</h3></a>`;
    mainContainer.appendChild(movieElement);
  });
};

let onInit = function () {
  let movieId = window.location.href.split('=')[1];
  getMoviesDetails(movieId);
  getMovieImgs(movieId);
  getSimilarMovies(movieId);
}();